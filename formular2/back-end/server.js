const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255), cnp VARCHAR(20), email  VARCHAR(20), data_inceput  VARCHAR(10), data_sfarsit  VARCHAR(10), varsta VARCHAR(3), gen VARCHAR(2))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
    gen: ""
  };
  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ

  if (!bilet.nume||!bilet.prenume||!bilet.varsta||!bilet.telefon||!bilet.email||!bilet.data_inceput||!bilet.data_sfarsit||!bilet.cnp) {
      console.log("Unul sau mai multe campuri nu au fost introduse!");
      error.push("Unul sau mai multe campuri nu au fost introduse");
} else {
    if (bilet.nume.length < 3 || bilet.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!bilet.nume.match("^[3-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (bilet.prenume.length < 3 || bilet.prenume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (bilet.cnp.length != 13) {
      console.log("CNP-ul trebuie sa fie de 13 cifre!");
      error.push("CNP-ul trebuie sa fie de 13 cifre!");
    } else if (!bilet.cnp.match("^[0-9]+$")) {
      console.log("CNP-ul trebuie sa contina doar cifre!");
      error.push("CNP-ul trebuie sa contina doar cifre!");
    }
    if (bilet.varsta.length < 1 || bilet.varsta.length > 3) {
      console.log("Varsta invalida!");
      error.push("Varsta invalida");
    } else if (!bilet.varsta.match("^[0-9]+$")) {
      console.log("Varsta trebuie sa contina doar cifre!");
      error.push("Varsta trebuie sa contina doar cifre!");
    } 
    
    if((+bilet.cnp.slice(1,3)+ +bilet.varsta)%100 != 20)
      {
        console.log("Varsta invalida!");
        error.push("Varsta invalida");
     }
    
    if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!bilet.email.match("^[a-zA-Z0-9.!#$%&’+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$"))
    {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    if(!bilet.data_inceput.match(/^\d{1,2}\/\d{1,2}\/\d{4}$/))
    {
      console.log("Data de inceput invalida!");
      error.push("Data de inceput invalida!  Format: zz/ll/aaaa");
    }
    if(!bilet.data_sfarsit.match(/^\d{1,2}\/\d{1,2}\/\d{4}$/))
    {
      console.log("Data de sfarsit invalida!");
      error.push("Data de sfarsit invalida! Format: zz/ll/aaa");
    }
    if(!(bilet.data_inceput<bilet.data_sfarsit))
    {
      console.log("Data de inceput trebuie sa fie inaintea celei de sfarsit!");
      error.push("Data de inceput trebuie sa fie inaintea celei de sfarsit!");
    }
    if(bilet.cnp[0]=="1"||bilet.cnp[0]=="3"||bilet.cnp[0]=="5")
    bilet.gen="M";
    else if(bilet.cnp[0]=="2"||bilet.cnp[0]=="4"||bilet.cnp[0]=="6")
    bilet.gen="F";
    else
    {
      console.log("CNP invalid!");
      error.push("CNP invalid!");
    }
  }
  if (error.length === 0) {

    const sql = `INSERT INTO abonamente_metrou (nume, prenume, telefon, cnp, email, data_inceput, data_sfarsit, varsta, gen) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.varsta,
        bilet.gen,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html